extern crate image;
extern crate rand;

use std::{env};
use std::cmp::Ordering;
use image::{GenericImageView, ImageBuffer, RgbaImage, Pixel, Rgba};
use rand::prelude::*;
use rand::distributions::StandardNormal;

fn main() {

    let mag: f64 = 7.0;
    let block_height: u32 = 10;
    let block_offset: f64 = 30.;
    let stride_mag: f64 = 0.1;

    let lag: f64 = 0.005;
    let mut lr: f64 = -7.;
    let mut lg: f64 = 0.;
    let mut lb: f64 = 3.;
    let std_offset: f64 = 10.;
    // let add = 39;

    let mean_abber = 10;
    let std_abber: f64 = 10.;


    let mut rng = rand::thread_rng();

    let mut line_off = 0;
    let mut stride = 0.;
    let mut yset = 0;

    let args: Vec<String> = env::args().collect();
    let source_image = image::open(&args[1])
        .expect("Error while opening the original image");
    let (x_min, y_min, x_max, y_max) = source_image.bounds();
    let mut new_img: RgbaImage = ImageBuffer::new(x_max, y_max);
    for y in y_min..y_max {
        for x in x_min..x_max {
            if rng.gen_range(0, block_height*(x_max-x_min)) == 0 {
                line_off = offset(block_offset);
                stride = StandardNormal.sample(&mut rng) * stride_mag;
                yset = y;
            }
            let stride_off: i64 = (stride * ((y as i64 - yset as i64) as f64)) as i64;
            let offx = offset(mag) + line_off + stride_off as i64;
            let offy = offset(mag);
            let src = source_image.get_pixel(
                wrap(x as i64 + offx, x_min as i64, x_max as i64),
                wrap(y as i64 + offy, y_min as i64, y_max as i64)
            );
            new_img.put_pixel(x, y, src);
        }
    }

    // step 2
    let mut new_img_1: RgbaImage = ImageBuffer::new(x_max, y_max);

    for y in y_min..y_max {
        for x in x_min..x_max {
            lr += StandardNormal.sample(&mut rng) * lag;
            lg += StandardNormal.sample(&mut rng) * lag;
            lb += StandardNormal.sample(&mut rng) * lag;
            let offx = offset(std_offset);

            let [r, _, _, a] = new_img.get_pixel(
                wrap(x as i64 + lr as i64 - offx as i64, x_min as i64, x_max as i64),
                wrap(y as i64, y_min as i64, y_max as i64)
            ).to_rgba().data;
            let g = new_img.get_pixel(
                wrap(x as i64 + lg as i64, x_min as i64, x_max as i64),
                wrap(y as i64, y_min as i64, y_max as i64)
            ).to_rgba().data[1];
            let b = new_img.get_pixel(
                wrap(x as i64 + lb as i64 + offx as i64, x_min as i64, x_max as i64),
                wrap(y as i64, y_min as i64, y_max as i64)
            ).to_rgba().data[2];

            let pixel = Rgba([
                r,//brighten(r, add),
                g,//brighten(g, add),
                b,//brighten(b, add),
                a]
            );

            new_img_1.put_pixel(x, y, pixel);
        }
    }

    // step 3
    for y in y_min..y_max {
        for x in x_min..x_max {
            let offx = mean_abber + offset(std_abber);
            let [r, _, _, a] = new_img_1.get_pixel(
                wrap(x as i64 + offx, x_min as i64, x_max as i64),
                wrap(y as i64, y_min as i64, y_max as i64)
            ).to_rgba().data;
            let g = new_img_1.get_pixel(
                wrap(x as i64, x_min as i64, x_max as i64),
                wrap(y as i64, y_min as i64, y_max as i64)
            ).to_rgba().data[1];
            let b = new_img_1.get_pixel(
                wrap(x as i64 - offx, x_min as i64, x_max as i64),
                wrap(y as i64, y_min as i64, y_max as i64)
            ).to_rgba().data[2];

            new_img_1.put_pixel(x, y, Rgba([r, g, b, a]));
        }
    }


    new_img_1.save(&args[2]).expect("Error while saving the result");
}

// fn brighten(r: u8, add: u8) -> u8 {
//     r - r * add / 255 + add
// }

fn wrap(x: i64, a: i64, b: i64) -> u32 {
    match x.cmp(&a) {
        Ordering::Less => {
            let mut result = x + b - a;
            if result < 0 {
                result = b + result;
            }
            result as u32
        },
        Ordering::Greater => {
            let mut result = x + a - b;
            if result < 0 {
                result = b + result;
            }
            result as u32
        },
        Ordering::Equal => {
            let result = x as u32;
            result
        }
    }
}

fn offset(m: f64) -> i64 {
    (StandardNormal.sample(&mut rand::thread_rng()) * m) as i64
}
